import { StyleSheet } from "react-native";

const styles = StyleSheet.create({

    //Almost all styling is found in this file, with a few inline exceptions in the components,
    // that were not considered too excessive.

    
    //Style for movie container in HomeScreen
    movieContainer: {
        flex: 1,
        width: '100%',
        height: '90%',
        alignItems: 'center',
        justifyContent: 'center',
    },
    // Style for movieItems in MovieItem
    movieItem: {
        fontSize: 24,
        marginBottom: 10,
        marginHorizontal: 5,
        borderRadius: 2,
        flexWrap: 'wrap',
        flexDirection: 'row',
        backgroundColor: "darkgray",

    },

    movieItemFavorited: {
        fontSize: 24,
        marginBottom: 10,
        marginHorizontal: 5,
        borderRadius: 2,
        flexWrap: 'wrap',
        flexDirection: 'row',
        backgroundColor: "darkgray",
    },
    //Style for movie scrollview in MovieItem
    movieList: {
        display: "flex",
        backgroundColor: "#3b3b3b",

        width: "95%",
        borderColor: "black",
        borderWidth: 2,
        borderRadius: 5,
        paddingTop: 5,
        contentContainerStyle: {
            justifyContent: "center",
            alignItems: "center",
        },
    },
    //Style for movie imagecontainer in MovieItem
    imageContainer: {
        width: 100,
        height: 150,
        marginRight: 5,
        borderWidth:1,
        borderColor: "black",
        borderRadius: 2,
    },
    //Style for movie info inside movieItem in MovieItem
    movieInfo:{ 
        flex: 1, 
        flexDirection: "row", 
        justifyContent: 'space-between',
        marginRight: 5,
    },
    movieInfoActors:{ 
        flex: 1, 
        flexDirection: "column", 
        justifyContent: 'center',
        textAlign: "center",
        marginRight: 5,
 
    },

    // Styling for movie info view
    movieInfoTitle:{
        flex: 1,
        flexDirection: "row",
        justifyContent: 'center',
        alignItems: 'center',
        flexWrap: 'wrap',
        marginRight: 5,
    },
    //Styling for movie info text
    movieTitleText:{
        textAlign:'center',
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: "black",

        marginTop: 1,
        fontSize: 16,
        fontWeight: "bold",
        
    },
    //Styling for actor tags
    movieActorText:{
        textAlign:'center',
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: "black",
        borderWidth: 1,
        borderRadius: 2,
        backgroundColor: "gray",
        margin: 2,
        
    },
    // Styling for favorites-button when not movie not favorited
    favButton:{
        textAlign:'center',
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: "black",
        borderWidth: 1,
        borderRadius: 2,
        backgroundColor: "orange",
        marginLeft: 12,
        


    },
    // Styling for favorites-button when movie is favorited
    unfavButton:{
        textAlign:'center',
        justifyContent: 'center',
        alignItems: 'center',
        borderColor: "black",
        borderWidth: 1,
        borderRadius: 2,
        backgroundColor: "yellow",
        marginLeft: 12,
    },
    //Genre-styling
    movieGenreText:{
        
        alignItems: 'center',
        borderColor: "black",
        borderWidth: 1,
        borderRadius: 2,
        backgroundColor: "lightgray",
        margin: 2,
        
    },

    //Styling for profile text
    profileHeader:{
        fontSize: 20,
        fontWeight: "bold",
    },
    profileText:{
        fontSize: 16,
    },

    //Styling to hide password unless 'Show Password' is toggled.
    hidePassword: {
        fontSize:16,
        textDecorationLine:"line-through",
        color: "transparent",
        textShadowColor: 'rgba(0, 0, 0, 0.75)',
        textShadowOffset: {width: -1, height: 1},
        textShadowRadius: 10,
        webkittouchcallout: "none",
        webkituserselect: "none",
        khtmluserselect: "none",
        mozuserselect: "none",
        msuserselect: "none",
        userselect: "none",

    },

    }

);

export default styles