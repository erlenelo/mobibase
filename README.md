# Project 4 - MobiBase #
This project is a prototype react-native mobile version of [Project 3 of Group 55, IT2810](https://gitlab.stud.idi.ntnu.no/it2810-h22/Team-55/oving-3). It allows the user to log into an already-existing account on the KRDB database from project 3, to browse and search all available movies listed in the DB, and also add movies to the user's favorite-list. 
## Table of Contents ##

- [Content and Functionality](#content-and-functionality)
- [User Manual](#user-manual)
- [Known Bugs](#known-bugs)
- [Technology](#technology)
- [Testing](#testing)
- [Sustainable Development and Web Accesibility](#sustainable-development-and-web-accessibility)
- [Git](#git)
- [Reflections](#reflections)

## Content and Functionality ##
The scope and functionality of this app has been scaled down from the original project 3, as it has only been developed by one person. The App nonetheless should cover almost all the basic functionality needed to operate key features of KRDB. It has also been developed with the intention of being responsive on most devices.

Upon opening the App, the user is greeted with a login page, to log into an existing user in the KRDB database. For the purposes of reviewing this app, a test user with available credentials has been created (and can be found further down in this document, under [User Manual](#user-Manual).

After Logging in, the user is met with the Home Screen, where all available movies in the database can be browsed.  The user can favorite these movies by tapping the "favorite" button on each movie item. When toggled, the button turns orange, and changes title to "unfavorite". The movie is then added as favorited to the user account. Clicking on the same button again will remove it from the user's favorite list. All favorited movies are shown in the Profile Screen, accessed via the navbar at the bottom of the screen. Here, the user can also 'un-favorite' movies, to remove them from their user. 

The navbar also contains a log-out button, that will navigate to the login-page, and log off the user.

## User manual ##
Application must be run through Expo Client. Run **expo start** in the terminal with a connected device or simulator.

For the purposes of testing, a testing account has been created to allow testers to log into the service. 

The following steps will take you through the full experience of the app:
1. Log in with these credentials (case-sensitive):
    - Username: MobiBase
    - Password: MobiBase

    (Pressing any other tabs in the navbar will not work until logged in, and will alert the user)

3. Browse all the movies in the database by scrolling down. The scrollview has a bug (see bug #1 in [Known Bugs](#known-bugs) for a workaround).

4. Add movie to favorites by tapping 'favorite'. The button will change to 'unfavorite'

5. Proceed to the Profile Screen via the right-most icon in the navbar (the profile avatar button)

6. Check if the movie has been added in the list below 'FAVORITED'. 

7. Tap the 'unfavorite'-button to remove it from the favorite-list.

8. Log out from the account by pressing the 'log-out' button in the navbar.

If you want to create your own user, you can do that by visiting the previous project [here](http://it2810-55.idi.ntnu.no/project3/). (Requires NTNU VPN)

## Known bugs ##
1. Pagination via offset and limit is seemingly not working as intended. In the previous project (which has the exact same, working, logic), offset was set to page * Page_Size(being 8), and limit being 8. This would return the correct amount of movies. In this project however, using the same offset/limit logic results in movies only being shown at page 0, and no movies on any other. Fiddling around with the offset/limit gave little results. 
As I have not been able to debug this, I have opted for a workaround by using scrollview with procedural loading if the user scrolls down, along with some control buttons. This is not ideal, but it works. Unfortunately it lacks logic to return the user to the bottom, from whence the scrollview expanded. **Workaround**: Load in the entire database with **Load All** for a smoother experience.

2. Some of the movie images are not loading. They do all load in the original project, so I am unsure why this happens. I believe this might be remedied when using a VPN, but I have not been able to test this. This is a minor bug, and does not limit any functionality.

3. When adding a favorite movie, the movie is added to the profile list. However, if that movie is removed via profile list, and the user returns to homescreen, the button is not updated to reflect the change. The button will still show "unfavorite", despite not being favorited in the database. Clicking on 'unfavorite' will query DB to remove the movie from favorites, but it is already removed. This is a minor bug, and does not affect the functionality of the app however.


## Technology ##
The project is built using the GRAND-stack (GraphQL, React(native), Apollo, Neo4j Database).
Expo was used to build the app, and the app is tested mainly on a physical Android device.
The backend of this project is reused from Project 3; a neo4j database with roughly 100 movies, with various relations of actors, genres, etc. The database also contains user accounts, and the original KRDB app has functionality implemented to create new user accounts dynamically. Apollo GraphQL is used to query the necessary information from the database.

For state management, Zustand is used as a global storage for all shared states that the components are dependent upon. It's an easy-to-use, lightweight storage much akin to redux/recoil, but without the need to wrap components in providers. It can be hooked in directly, wherever needed.

Routing is done via the native react Navigation Stack, as a footer bar that can be cycled through to navigate between the different screens (login(or out), Home, Profile).

## Testing ##
While automated tests/integration tests would have benefited the project, a concious decision was made not to focus on it. The workload has been extensive already for one person, and I chose to dedicate the time to other aspects of the project. Extensive manual testing has been done however, and the app has been tested primarily physically on Android (Samsung Galaxy S21 Ultra), but also in Android Studio's simulator (Pixel 2 and Pixel 3).

## Sustainable Development and Web Accessibility ##
**Sustainable Development**
The original plan for this project was to use pagination to limit the query traffic, and items inside the scrollview. However, as mentioned in [Known Bugs](#known-bugs), pagination ended up not working as intended, and would not fetch correct results. The workaround was to implement a scrollview with procedural loading, and a load all button, but even here the pagination bug is present. As such, there is presumably unecessary fetching of data, and the app is not sustainable in the long run. However, with the use of caching from appollo, this is somewhat remedied, as the app will not fetch the same data twice.

The colours used are slightly darker, to reduce power-consumption. While the app could use even darker colours to optimalize this feature even more, I instead opted for a more pleasant experience for the user.

**Web Accessibility**
Contrast in colours in actively used to make text and features stand out. All pages are accessible via one single nav bar. Alerts have also been implemented as input-helpers, namely when trying to access a page without being logged in, or when trying to log in with incorrect credentials. 

Given more time, I would have liked to implement a more accessible design for the scrollview, as the loading to fetch more entries puts the user back to the top of scrollview.

## Git ##
Git has been used extensively throughout the development of this App, even though it's only me working on it. This has been done to hopefully show that I at least know how to use it correctly as if I was working in a team. First and foremost, all issues should have links to relevant commits and merges, along with descriptions. I have made respective branches for each issue, and merged them into the main branch upon reaching satisfactory resolution to the issues. 

## Reflections ##
All in all, this has been a very enjoyable project to work on. It's nice to put everything learnt about React up to the test, especially trying to figure out things all alone. It's slow, if not sometimes outright frustrating, but managing to sort out the problems and fixing them is very rewarding. I have also found out that I need to test features that I am implementing more thoroughly before jumping into the next issue, to avoid having to backtrack and fix bugs that I could have avoided.

Nonetheless I am content with the functionality I have been able to implement, and I am happy with the end result. If I had more time, I would have liked to implement more features, such as a search bar, and ways to view description for movie entries. Automated testing would be a logical next step (and preferably before adding new features). Finally, I wish I would've been able to primarily solve the pagination bug if there was more time, as I believe that this would have made the app more sustainable and functional.








