import * as React from 'react';
import { Key } from 'react';
import { View, ScrollView, } from 'react-native';
import Style from '../styles/Style';

import {useQuery, gql} from '@apollo/client';
import useInfoStore from '../store/InfoStorage';
import { FavItem } from './FavItem';

// This is the component for ProfileScreen, that takes in FavItem as a child component, inside scrollview.
function FavoritesContainer() {
    const username = useInfoStore((state)=>state.username);

    //Altered query to get all favorite movies from user
    function GetUserFavorites(Username: String): any {
        const GET_USER_FAVORITES = gql`
          query GetUserFavorites ($username: String!){
            users (where: {username: $username}) {
              favorites {
                title
                rating
                img
                year
                actors {
                    name
                }
                genres {
                    name
                }
              }
            }
          }`
      
        const { error, loading, data } = useQuery(
          GET_USER_FAVORITES, {
            variables:{
            username: Username,
          },
          pollInterval: 100,
        });
          
        if (typeof data !== 'undefined') {
          if (data.users.length != 0) {
            // Process the retrieved data into a object with a associated movies array
            const value = data.users[0].favorites
            return ({movies: value})
          } else {
            return []
          }
        } else {
            return []
        }
      }

      // Get user favorites
    const favoritesData = GetUserFavorites(username);
      
    return(
        <View style={Style.movieContainer}>
            <ScrollView 
            style={Style.movieList} 
            scrollEventThrottle={500}
            >
            {favoritesData?.movies?.map((movie: { id: Key | null | undefined; }) => (
                
            <FavItem key={movie.id} movie={movie} />
            
            ))}
            </ScrollView>
        </View>
            
    );
}
export default FavoritesContainer;
