import * as React from 'react';
import { Key, useState } from 'react';
import { View, Text, ScrollView, Button } from 'react-native';
import Style from '../styles/Style';
import { MovieItem } from './MovieItem';
import {useQuery, gql} from '@apollo/client';


// This is the component for HomeScreen, that takes in MovieItem as a child component, inside scrollview.
function MovieContainer() {
    //Altered queries to also receive actors and genres
    const SORTED_BY_RATING = gql`
    query Movies($limit: Int!, $offset: Int!){
      movies(options: {limit: $limit, offset: $offset, sort: {rating:DESC} }) {
        title
        rating
        img
        year
        actors {
            name
        }
        genres {
            name
        }
      

      }
    }`

    const SORTED_BY_YEARS = gql`
    query Movies($limit: Int!, $offset: Int!){
      movies(options: {limit: $limit, offset: $offset, sort: {year:DESC} }) {
        title
        rating
        img
        year
        actors {
            name
        }
        genres {
            name
        }
      }
    }`
  
    //Pagination:
    const[page, setPage] = useState(1);
    const PAGE_SIZE = 10;

    //Sorting, change query based on state of toggler:
    const [query, setQuery]= useState(SORTED_BY_RATING);
    const [toggleValue, setToggleValue] = useState(true); //false=shows by rating. true=shows by year
    const toggler = () => {
      toggleValue ? setToggleValue(false): setToggleValue(true);
      if(toggleValue) {setQuery(SORTED_BY_YEARS);}
      else{setQuery(SORTED_BY_RATING);}
    }

    // Fetching (This is where the offset/limit bug is encountered (see documentation)):
    const { error, loading, data} = useQuery(query, {variables:{limit: PAGE_SIZE*page, offset:0}, partialRefetch: true, pollInterval: 500});
    if (loading) return <Text>Loading...</Text>;
    if (error) return <Text>${error.message}</Text>;
   
    console.log(page + " of " + PAGE_SIZE)
    console.log(data.movies.length)

    // Expanding list when near bottom. This is a workaround since pagination is not working as originally intended.
    // Also lacking logic to return to position in list after expanding/fetching.
    const isCloseToBottom = ({layoutMeasurement, contentOffset, contentSize}) => {
        const paddingToBottom = 5;
        return layoutMeasurement.height + contentOffset.y >=
          contentSize.height - paddingToBottom;
    };

    return(
        <View style={Style.movieContainer}>
            
            <ScrollView style={Style.movieList} onScroll={({nativeEvent})=>{
              if(data.movies.length<104 || page<11){
                if (isCloseToBottom(nativeEvent)) {
                    setPage(page+1);
                }
              }
            }}
            scrollEventThrottle={500}
            >
            {data?.movies?.map((movie: { id: Key | null | undefined; }) => (
                
            <MovieItem key={movie.id} movie={movie} />
            
            ))}
            </ScrollView>
            <View style={{flexDirection: "row", margin: 3}}>
              <View style={{margin:3}}>
              <Button title = "Collapse" onPress = {()=>setPage(1)}></Button>
              </View>
              <View style={{margin:3}}>
              {toggleValue ? <Button title = "Sort by year" onPress={toggler}/> 
              : 
              <Button title = "Sort by rating" onPress={toggler}/>}
              </View>
              <View style={{margin:3}}>
              <Button title = "Load All" onPress = {()=>setPage(10)}></Button>

              </View>
            </View>
            
        </View>
    );
}
export default MovieContainer;
