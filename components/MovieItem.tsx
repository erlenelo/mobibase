import * as React from 'react'
import { View, Text, Image, Pressable} from 'react-native'
import useInfoStore from '../store/InfoStorage'
import {AddUserFavorite, CheckUserFavorites, RemoveUserFavorite} from '../api/UserQuery'


import Style from '../styles/Style'
import { useEffect, useState } from 'react'


// This is the component for movie items inside the scrollview of HomeScreen.
export const MovieItem = ({movie} : {movie:any}) => {
const {title, rating, img} = movie;
// Set and remove favorites from API
const [setFavoriteAPI, setFavoriteAPIExecution] = AddUserFavorite()
const [removeFavoriteAPI, removeFavoriteAPIExecution] = RemoveUserFavorite()
const username = useInfoStore((state)=>state.username)
// Check if movie is favorited
const userFavorites = CheckUserFavorites(username, title)
// If favorited, set state to true to toggle favorite-button
const [isFavorite, setIsFavorite] = useState<boolean>(false);

//UseEffect to check if movie is favorited
useEffect(()=>{
    if(userFavorites){
        setIsFavorite(true)
    } else {
        setIsFavorite(false)
    }
},[])
// Function to handle favorite-button
const handleFavorite = () => {
    const variables ={variables: {username: username, movie: title}}
    if(isFavorite){
        console.log("REMOVING")
        setIsFavorite(false);
        removeFavoriteAPI(variables) 
    } else {
        console.log("ADDING")
        setIsFavorite(true);
        setFavoriteAPI(variables) 
    }
}
    return (
        <View key = {movie.id}>
            <View style={Style.movieItemFavorited}>
                <Image style = {Style.imageContainer} source={{uri: img}}/>
                <View style={[Style.movieInfo, {
                    flexDirection: "column"
                    }]}>
                    <View style={Style.movieInfoTitle} >
                        <Text style={Style.movieTitleText}> {title}</Text>
                        
                    </View>
                    <View style={Style.movieInfoTitle}>
                        <Text>{movie.year}    </Text>
                        <Text>Score: {rating}</Text>
                        {isFavorite ? 
                        <Pressable style={Style.favButton} onPress ={handleFavorite}>
                            <Text> Unfavorite </Text>
                        </Pressable>
                        :
                        <Pressable style={Style.unfavButton} onPress ={handleFavorite}>
                            <Text> Favorite </Text>
                        </Pressable>}
                    </View>
                    <View style={Style.movieInfoTitle}>
                    {movie.genres.map((genre:any)=>(
                            <Text style={Style.movieGenreText}> {genre.name} </Text>
                        ))}
                    </View>
                    <View style={Style.movieInfoTitle}>
                            {movie.actors.map((actor:any)=>(
                                <Text style={Style.movieActorText}> {actor.name} </Text>
                            ))}
                            
                        </View>
                    </View>
            </View>
    </View>
                                    
    )  
}
