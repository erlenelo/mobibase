import AsyncStorage  from '@react-native-async-storage/async-storage';
import create from 'zustand';
import { devtools, persist } from 'zustand/middleware';

export interface InfoState {
    username: string;
    setUsername: (username: string) => void;
    password: string;
    setPassword: (password: string) => void;
    isLoggedIn: boolean;
    setIsLoggedIn: (isLoggedIn: boolean) => void;
    page: number;
    setPage: (page: number) => void;
    movieTitle: string;
    setMovieTitle: (movieTitle: string) => void;

}

// This is the global state manager for the app. It is used to store all states that are passed across the app.
const useInfoStore = create<InfoState>()(
    persist(
        devtools(
        (set,get) => ({
            username: '',
            setUsername: (username) => set(()=>({ username })),
            password: '',
            setPassword: (password) => set(()=>({ password })),
            isLoggedIn: false,
            setIsLoggedIn: (isLoggedIn) => set(()=>({ isLoggedIn })),
            page: 0,
            setPage: (page) => set(()=>({ page })),
            movieTitle: '',
            setMovieTitle: (movieTitle) => set(()=>({ movieTitle })),
        })),
        { 
            name: 'info-storage',
            getStorage: () => AsyncStorage
        }
    ));

    export default useInfoStore;