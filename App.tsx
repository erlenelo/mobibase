import {
  ApolloClient,
  InMemoryCache,
  ApolloProvider,
  useQuery,
  gql,
  HttpLink,
  from
} from "@apollo/client";
import {onError} from '@apollo/client/link/error';


const errorLink = onError(({ graphQLErrors, networkError }) => {
  if (graphQLErrors) {
    graphQLErrors.map(({ message, locations, path }) => {
      alert(`Graphql error ${message}`);
    });
  }
});

const link = from([
  errorLink,
  new HttpLink({ uri: "https://it2810-55s.idi.ntnu.no/graphql" }),
]);

const client = new ApolloClient({
  cache: new InMemoryCache(),
  link: link,

});

import { StyleSheet, Text, View } from 'react-native';

import MainContainer from './navigation/MainContainer';

export default function App() {
  return (
    <ApolloProvider client={client}>
    <MainContainer/>
    </ApolloProvider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
