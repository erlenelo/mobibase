import * as React from 'react';

import { NavigationContainer} from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import IonIcons from 'react-native-vector-icons/Ionicons';

//Screens
import HomeScreen from './screens/HomeScreen';
import LoginScreen from './screens/LoginScreen';
import ProfileScreen from './screens/ProfileScreen';
import useInfoStore from '../store/InfoStorage';

// Names of screens
const homeName = 'Home';
const profileName = 'Profile';

// Making loginName a variable so that it can be changed to 'Log out' if user is logged in
let loginName = 'Login';
const logoutName = "Log out";
const Navbar = createBottomTabNavigator();

//This is the main container, that holds all screens and the navbar
export default function MainContainer(){
    // Get the current state of isLoggedIn from zustand storage
    const [isLoggedIn, setIsLoggedIn] = useInfoStore(((state)=>[state.isLoggedIn, state.setIsLoggedIn]));
    
    if (isLoggedIn){
        loginName = logoutName;
    }

    return(
        <NavigationContainer>
            <Navbar.Navigator
                initialRouteName={loginName}
                screenOptions={({route}) => ({
                    tabBarIcon: ({focused, color, size}) => {
                        let iconName;
                        if(isLoggedIn == true){
                        
                        
                            if(route.name === loginName){
                                iconName = focused ? 'ios-log-out' : 'ios-log-out-outline';
                            } else if(route.name === homeName){
                                iconName = focused ? 'home' : 'home-outline';
                            } else if(route.name === profileName){
                                iconName = focused ? 'person' : 'person-outline';    
                            }
                        } else {
                            if(route.name === loginName){
                                iconName = focused ? 'ios-log-in' : 'ios-log-in-outline';
                            } else if(route.name === homeName){
                                iconName = focused ? 'home' : 'home-outline';
                            } else if(route.name === profileName){
                                iconName = focused ? 'person' : 'person-outline';    
                            }
                        }
                        
                        return <IonIcons name={iconName} size={size} color={color} />
                        
                    },
                
                    headerTitleAlign: 'center',
                    
                    headerStyle: {
                        backgroundColor: '#3b3b3b',
                        height: 80,   
                    },
                    headerTitleStyle: {
                        color: 'tomato',
                        fontSize: 30,
                        fontWeight: 'bold',
                    },
                    tabBarStyle: {
                        backgroundColor: '#3b3b3b',
                        height: 60,

                    },
                    tabBarLabelStyle: { fontSize: 12},
                    tabBarActiveTintColor: 'tomato',
                    
                    
                })}>
                <Navbar.Screen name={loginName} component={LoginScreen} 
                listeners={({ navigation, route }) => ({
                    tabPress: (e) => {
                      if(isLoggedIn == true){
                        setIsLoggedIn(false);
                        loginName = 'Login';
                        alert ("You have logged out.");
                      } 
                        
                    },
                  })}
                />
                <Navbar.Screen name={homeName} component={HomeScreen} 
                listeners={({ navigation, route }) => ({
                    tabPress: (e) => {
                      if(isLoggedIn == false){
                        // Prevent navigation unless logged in
                            e.preventDefault();
                            alert ("You must be logged in to view this page. Log in with your username and password.");
                      } 
                    },
                  })}
                />

                <Navbar.Screen name={profileName} component={ProfileScreen} 
                listeners={({ navigation, route }) => ({
                    tabPress: (e) => {
                        if(isLoggedIn == false){
                            // Prevent navigation unless logged in
                            e.preventDefault();
                            alert ("You must be logged in to view this page. Log in with your username and password.");
                        } 
                    },
                })}
                />

            </Navbar.Navigator>
        </NavigationContainer>

    )
}
