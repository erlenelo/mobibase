import { View, Text, Button, } from "react-native";
import React, {useState} from "react";
import useInfoStore from "../../store/InfoStorage";
import Style from "../../styles/Style";
import FavoriteContainer from "../../components/FavoriteContainer";


export default function ProfileScreen({navigation}) {

    // Zustand storage:
    const username = useInfoStore((state)=>state.username);
    const password = useInfoStore((state)=>state.password);

    const [showPassword, setShowPassword] = useState(true);
    const [btnText, setBtnText] = useState("Show password");
    const handleClick = () => {setShowPassword(current=>!current);
        setBtnText(current=>current==="Show password"?"Hide password":"Show password");
    }


    return(
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: "gray"}}>
            <Text style={Style.profileHeader}>USER PROFILE</Text>
            
            <Text style={Style.profileText}>Username: {username}</Text>
            
            <View style={{flexDirection: "row"}}>
                <Text style={Style.profileText}>Password: </Text> 
                <Text style={showPassword ? Style.hidePassword : Style.profileText}>{password}</Text>
            </View>
            <View style={{margin:5}}>
            <Button title={btnText} onPress={handleClick}></Button>
            </View>
            <Text style={Style.profileHeader}>FAVORITES</Text>
            
            <FavoriteContainer/>
            
        </View>
    )
}