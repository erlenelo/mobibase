import { View, Text, TextInput, Button, TextInputChangeEventData, NativeSyntheticEvent, } from "react-native";
import React, {HtmlHTMLAttributes, useEffect, useState} from "react";
import useInfoStore from "../../store/InfoStorage";
import CheckValidUser, { GetUserFavorites } from "../../api/UserQuery"


export default function LoginScreen({navigation}) {
    const [usernameInput,setUsernameInput] = React.useState("");
    const [passwordInput,setPasswordInput] = React.useState("");

    // Update username and password to zustand storage:
    const [username, updateUserName] = useInfoStore(((state)=>[state.username, state.setUsername]));
    const [password, updatePassword] = useInfoStore(((state)=>[state.password, state.setPassword]));
    const [isLoggedIn, setIsLoggedIn] = useInfoStore(((state)=>[state.isLoggedIn, state.setIsLoggedIn]));

    //Check for valid login:
    const checkValid = CheckValidUser(usernameInput,passwordInput);

    // Listener for username and password input:
    const handleUsername = (e: NativeSyntheticEvent<TextInputChangeEventData>) => {
        setUsernameInput(e.nativeEvent.text);}
    const handlePassword = (e: NativeSyntheticEvent<TextInputChangeEventData>) => {
        setPasswordInput(e.nativeEvent.text);}
        
    // Handling login, checking if user exists in database and password is correct
    const handleLogin = () => {
        if(checkValid){
        updateUserName(usernameInput);
        updatePassword(passwordInput);
        setIsLoggedIn(true);
       
        // Then navigate to home screen
        navigation.navigate("Home");
        console.log(username + " " + password);

        }
        else{
            console.log("Invalid login");
            alert("Invalid login");
        }

    }
    return(
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: "gray"}}>
            <View style={{width: "90%", marginBottom: "5%", marginTop:"-20%"}}>
                <Text style={{textAlign: "center", fontSize:24, fontWeight: "bold", color: "3b3b3b", }}>Welcome! {"\n"} Log into your KRDB-account to browse the database:</Text>
            </View>
            <View style={{borderWidth:2, borderRadius:3, width: "50%", margin: 5, backgroundColor: "lightgray"}}>
                <TextInput onChange ={handleUsername} style={{textAlign: "center"}} placeholder = "Username"></TextInput>
            </View>
            <View style={{borderWidth:2, borderRadius:3, width: "50%", margin: 5, backgroundColor: "lightgray"}}>
                <TextInput onChange={handlePassword} style={{textAlign: "center"}} placeholder = "Password"></TextInput>
            </View>
            <Button title = "Log in" onPress = {handleLogin}></Button>

        </View>
    )
}