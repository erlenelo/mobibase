import { View, Text} from "react-native";
import React, {useState} from "react";
import MovieContainer from "../../components/MovieContainer";



export default function HomeScreen({navigation}) {

    return(
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: "gray"}}>
            <Text style={{fontSize: 30, fontWeight: "bold"}}>BROWSE ALL MOVIES</Text>
                <MovieContainer/>

        </View>
    )
}
